﻿using System.Collections.Generic;
using UnityEngine;

public static class MeshCutter
{
    private static int[] VerticesOfSubtriangle1 = new int[3];
    private static int[] VerticesOfSubtriangle2 = new int[3];
    private static int[] VerticesOfSubQuad = new int[6];

    private static int[] triIndicies = new int[3];
    private static float[] VerticiesLerp = new float[3];

    private static MeshWrapper meshWrapperThis;
    private static PlaneWrapper planeCutterMathThis;

    private static List<int> subMeshIndecies = new List<int>();

    public static List<int> SubMeshIndecies
    {
        get
        {
            return subMeshIndecies;
        }
    }

    public static void Cut( MeshWrapper meshWrapper, PlaneWrapper planeCutterMath )
    {
        subMeshIndecies.Clear();
        meshWrapperThis = meshWrapper;
        planeCutterMathThis = planeCutterMath;

        int triCount = meshWrapper.triangles.Length - 2;

        for ( int triOffset = 0; triOffset < triCount; triOffset += 3 )
        {
            triIndicies[0] = meshWrapper.triangles[triOffset];
            triIndicies[1] = meshWrapper.triangles[1 + triOffset];
            triIndicies[2] = meshWrapper.triangles[2 + triOffset];

            VerticiesLerp[0] = planeCutterMath.LineIntersect( meshWrapper.wsVertices[triIndicies[0]], meshWrapper.wsVertices[triIndicies[1]] );
            VerticiesLerp[1] = planeCutterMath.LineIntersect( meshWrapper.wsVertices[triIndicies[1]], meshWrapper.wsVertices[triIndicies[2]] );
            VerticiesLerp[2] = planeCutterMath.LineIntersect( meshWrapper.wsVertices[triIndicies[2]], meshWrapper.wsVertices[triIndicies[0]] );

            bool[] wasLineHit = new bool[3];
            wasLineHit[0] = VerticiesLerp[0] > 0f && VerticiesLerp[0] < 1f;
            wasLineHit[1] = VerticiesLerp[1] > 0f && VerticiesLerp[1] < 1f;
            wasLineHit[2] = VerticiesLerp[2] > 0f && VerticiesLerp[2] < 1f;

            if ( wasLineHit[0] || wasLineHit[1] || wasLineHit[2] )
            {
                if ( wasLineHit[0] && wasLineHit[1] )
                {
                    CutTriangleByLines( 0 );
                } else
                if ( wasLineHit[1] && wasLineHit[2] )
                {
                    CutTriangleByLines( 1 );
                } else
                if ( wasLineHit[0] && wasLineHit[2] )
                {
                    CutTriangleByLines( 2 );
                } else
                if ( wasLineHit[1] )
                {
                    CutTriangleByLineAndVertex( 0 );
                } else
                if ( wasLineHit[2] )
                {
                    CutTriangleByLineAndVertex( 1 );
                }
                else
                {
                    CutTriangleByLineAndVertex( 2 );
                }
            }
            else
            {
                if ( planeCutterMath.PointSide( meshWrapper.wsVertices[triIndicies[0]] ) > 0f )
                {
                    meshWrapper.trisRight.AddRange( triIndicies );
                }
                else
                {
                    meshWrapper.trisLeft.AddRange( triIndicies );
                }
            }
        }
    }

    private static void CutTriangleByLines( int offset )
    {
        int i0 = offset % 3;
        int i1 = ( 1 + offset ) % 3;
        int i2 = ( 2 + offset ) % 3;

        int indexHit0 = meshWrapperThis.AddLerpVertex( triIndicies[i0], triIndicies[i1], VerticiesLerp[i0] );
        int indexHit1 = meshWrapperThis.AddLerpVertex( triIndicies[i1], triIndicies[i2], VerticiesLerp[i1] );

        AddSubMeshIndex( indexHit0 );
        AddSubMeshIndex( indexHit1 );
    
        VerticesOfSubtriangle1[0] = indexHit0;
        VerticesOfSubtriangle1[1] = triIndicies[i1];
        VerticesOfSubtriangle1[2] = indexHit1;
    
        VerticesOfSubQuad[0] = triIndicies[i0];
        VerticesOfSubQuad[1] = indexHit0;
        VerticesOfSubQuad[2] = indexHit1;
        VerticesOfSubQuad[3] = triIndicies[i0];
        VerticesOfSubQuad[4] = indexHit1;
        VerticesOfSubQuad[5] = triIndicies[i2];

        if ( planeCutterMathThis.PointSide( meshWrapperThis.wsVertices[triIndicies[i1]] ) > 0f )
        {
            meshWrapperThis.trisRight.AddRange( VerticesOfSubtriangle1 );
            meshWrapperThis.trisLeft.AddRange( VerticesOfSubQuad );
        }
        else
        {
            meshWrapperThis.trisLeft.AddRange( VerticesOfSubtriangle1 );
            meshWrapperThis.trisRight.AddRange( VerticesOfSubQuad );
        }
    }

    private static void CutTriangleByLineAndVertex( int offset )
    {
        int i0 = offset % 3;
        int i1 = ( 1 + offset ) % 3;
        int i2 = ( 2 + offset ) % 3;

        int indexHit = meshWrapperThis.AddLerpVertex( triIndicies[i0], triIndicies[i1], VerticiesLerp[i0] );

        AddSubMeshIndex( indexHit );
    
        VerticesOfSubtriangle1[0] = triIndicies[i0];
        VerticesOfSubtriangle1[1] = indexHit;
        VerticesOfSubtriangle1[2] = triIndicies[i2];
    
        VerticesOfSubtriangle2[0] = indexHit;
        VerticesOfSubtriangle2[1] = triIndicies[i1];
        VerticesOfSubtriangle2[2] = triIndicies[i2];

        if ( planeCutterMathThis.PointSide( meshWrapperThis.wsVertices[triIndicies[i0]] ) > 0f )
        {
            meshWrapperThis.trisRight.AddRange( VerticesOfSubtriangle1 );
            meshWrapperThis.trisLeft.AddRange( VerticesOfSubtriangle2 );
        }
        else
        {
            meshWrapperThis.trisLeft.AddRange( VerticesOfSubtriangle1 );
            meshWrapperThis.trisRight.AddRange( VerticesOfSubtriangle2 );
        }
    }

    private static void AddSubMeshIndex( int index )
    {
        int newIndex = index - meshWrapperThis.vertexCount;
        Vector3 compVec = meshWrapperThis.verticesCut[newIndex];

        subMeshIndecies.Add( index );
    }
}