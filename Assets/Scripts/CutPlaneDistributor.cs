﻿using System;
using UnityEngine;

public class CutPlaneDistributor : MonoBehaviour
{
    private CuttableMesh[] cuttableMeshes;

    public void destribute( Transform planeCutterTransform )
    {
        updateCattableArrayMesh();

        Array.ForEach( cuttableMeshes, cuttableMeshe => cuttableMeshe.Cut( planeCutterTransform ) );
    }

    private void updateCattableArrayMesh()
    {
        cuttableMeshes = GetComponentsInChildren<CuttableMesh>();
    }
}
