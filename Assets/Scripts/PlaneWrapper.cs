﻿using UnityEngine;

public class PlaneWrapper
{
    public Vector3 position;
    public Vector3 normal;

    public PlaneWrapper( Transform transform )
    {
        position = transform.position;
        normal = transform.up;
    }

    public float LineIntersect( Vector3 lineStart, Vector3 lineEnd )
    {
        return dotWithNormal( position - lineStart ) / dotWithNormal( lineEnd - lineStart );
    }

    public float PointSide( Vector3 point )
    {
        return dotWithNormal( point - position );
    }

    private float dotWithNormal( Vector3 rhs )
    {
        return Vector3.Dot( normal, rhs );
    }
}