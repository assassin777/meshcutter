﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(LineRenderer))]
public class CutPlaneDeterminer : MonoBehaviour
{
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField] private CutPlaneDistributor cutPlaneDistributor;

    private Vector3 downPos;
    private Vector3 upPos;

    bool down = false;

    private void Awake()
    {
        lineRenderer.sortingOrder = 1;
        lineRenderer.material = new Material( Shader.Find( "Sprites/Default" ) );
        lineRenderer.material.color = Color.red;
        lineRenderer.enabled = false;
    }

    private void Update()
    {
        if ( down )
        {
            lineRenderer.SetPosition(0, downPos);
            lineRenderer.SetPosition(1, WorldMousePosition());
        }

        if (Input.GetMouseButtonDown(0))
        {
            lineRenderer.enabled = true;

            lineRenderer.SetPosition(0, Vector3.zero );
            lineRenderer.SetPosition(1, Vector3.zero );

            downPos = WorldMousePosition();
            down = true;
        } else
        if ( down && Input.GetMouseButtonUp(0) )
        {
            lineRenderer.enabled = false;

            down = false;
            upPos = WorldMousePosition();

            if ( downPos == upPos )
                return;

            CreateCutPlane( downPos, upPos );
        }
    }

    private Vector3 WorldMousePosition()
    {
        Vector3 _vector3 = Input.mousePosition;
        _vector3.z = 1;
        return Camera.main.ScreenToWorldPoint( _vector3 );
    }

    private void CreateCutPlane( Vector3 downPos, Vector3 upPos )
    {
        Vector3 center = Vector3.Lerp( downPos, upPos, 0.5f );
        Vector3 cut = ( upPos - downPos ).normalized;
        Vector3 foward = ( center - transform.position ).normalized;
        Vector3 normal = Vector3.Cross( foward, cut ).normalized;

        GameObject planeCutter = new GameObject( "planeCutter" );
        Transform planeCutterTransform = planeCutter.transform;

        planeCutterTransform.position = center;
        planeCutterTransform.up = normal;
        float angleFoward = Vector3.Angle( planeCutterTransform.forward, foward );
        planeCutterTransform.RotateAround( center, normal, normal.y < 0f ? -angleFoward : angleFoward );

        cutPlaneDistributor.destribute( planeCutterTransform );
    }
}
