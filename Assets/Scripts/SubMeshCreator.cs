﻿using System.Collections.Generic;
using UnityEngine;

public static class SubMeshCreator
{
    private static int[] subMeshIndeciesSorted;

    public static void Create( MeshWrapper meshWrapper, PlaneWrapper planeCutterMath, Quaternion planeRotation, List<int> subMeshIndecies )
    {
         if ( subMeshIndecies.Count == 0 )
            return;

         SortSubMeshIndecies( meshWrapper, planeRotation, subMeshIndecies.ToArray() );

         Vector3 normal = Quaternion.Inverse( meshWrapper.transform.rotation ) * planeCutterMath.normal;
         Vector3 invNormal = -normal;

         int[] subMeshUpperOrder = new int[subMeshIndeciesSorted.Length];
         int[] subMeshLowerOrder = new int[subMeshIndeciesSorted.Length];

         for ( int i = 0; i < subMeshIndeciesSorted.Length; i++ )
         {
            subMeshUpperOrder[i] = meshWrapper.AddSubMeshVertex( subMeshIndeciesSorted[i], invNormal );
            subMeshLowerOrder[i] = meshWrapper.AddSubMeshVertex( subMeshIndeciesSorted[i], normal );
         }

         int capOderCount = subMeshUpperOrder.Length;
         for ( int i = 2; i < capOderCount; i++ )
         {
            meshWrapper.trisRight.Add( subMeshUpperOrder[0] );
            meshWrapper.trisRight.Add( subMeshUpperOrder[i - 1] );
            meshWrapper.trisRight.Add( subMeshUpperOrder[i] );

            meshWrapper.trisLeft.Add( subMeshLowerOrder[0] );
            meshWrapper.trisLeft.Add( subMeshLowerOrder[i] );
            meshWrapper.trisLeft.Add( subMeshLowerOrder[i - 1] );
         }
    }

    private static void SortSubMeshIndecies( MeshWrapper meshWrapper, Quaternion planeRotation, int[] subMeshIndecies )
    {
        Quaternion inversePlaneRotation = Quaternion.Inverse( planeRotation );
        Vector3 cutMeshPos = meshWrapper.transform.position;
        Vector2[] wsSubMeshVerticesXZ = new Vector2[subMeshIndecies.Length];

        for ( int i = 0; i < subMeshIndecies.Length; i++ )
        {
            Vector3 wsSubMeshVerticesPos = subMeshIndecies[i] < meshWrapper.vertexCount ? meshWrapper.wsVertices[subMeshIndecies[i]] : meshWrapper.wsVerticesCut[subMeshIndecies[i] - meshWrapper.vertexCount];
            wsSubMeshVerticesPos = inversePlaneRotation * ( wsSubMeshVerticesPos - cutMeshPos );
            wsSubMeshVerticesXZ[i] = new Vector2( wsSubMeshVerticesPos.x, wsSubMeshVerticesPos.z );
        }

        int[] sorted = new int[subMeshIndecies.Length];
        for ( int i = 0; i < sorted.Length; i++ )
        {
            sorted[i] = i;
        }

        int lowestIndex = FindLowestIndex( wsSubMeshVerticesXZ, sorted );

        if ( lowestIndex != 0 )
            Swap( sorted, 0, lowestIndex );

        int[] angles = CalcAngles( wsSubMeshVerticesXZ, sorted );

        SortByAngles( sorted, angles );
        SortForStart( sorted, angles, wsSubMeshVerticesXZ );
        SortForEnd( sorted, angles, wsSubMeshVerticesXZ );
        
        subMeshIndeciesSorted = new int[sorted.Length];

        for ( int i = 0; i < sorted.Length; i++ )
        {
            int sortIndex = sorted[i];
            if ( sortIndex >= 0 )
            {
                subMeshIndeciesSorted[i] = subMeshIndecies[sortIndex];
            }
        }
    }

    private static int FindLowestIndex( Vector2[] wsSubMeshVerticesXZ, int[] sorted )
    {
        int lowestIndex = 0;
        Vector2 lowestVec = wsSubMeshVerticesXZ[sorted[lowestIndex]];

        for ( int i = 1; i < sorted.Length; i++ )
        {
            if ( SortLowY( wsSubMeshVerticesXZ[sorted[i]], lowestVec ) )
            {
                lowestIndex = i;
                lowestVec = wsSubMeshVerticesXZ[sorted[lowestIndex]];
            }
        }

        return lowestIndex;
    }

    private static int[] CalcAngles( Vector2[] wsSubMeshVerticesXZ, int[] sorted )
    {
        int[] angles = new int[sorted.Length];
        Vector2 sVec = wsSubMeshVerticesXZ[sorted[0]];

        for ( int i = 1; i < sorted.Length; i++ )
        {
            Vector2 vec = wsSubMeshVerticesXZ[sorted[i]];
            float dot = Vector2.Dot( Vector2.up, ( vec - sVec ).normalized );

            if ( sVec.x <= vec.x )
            {
                angles[sorted[i]] = (int)( dot );
            }
            else
            {
                angles[sorted[i]] = (int)( ( 2f - dot ) );
            }

            if ( angles[sorted[i]] < 0 )
                angles[sorted[i]] = 0;
        }

        angles[sorted[0]] = -1;

        return angles;
    }

    private static void SortByAngles( int[] index, int[] value )
    {
        int pos = 1;

        while ( pos < index.Length )
        {
            if ( value[index[pos]] >= value[index[pos - 1]] )
                pos++;
            else
            {
                Swap( index, pos, pos - 1 );
                if ( pos > 1 )
                    pos--;
                else
                    pos++;
            }
        }
    }

    private static void SortForStart( int[] index, int[] value, Vector2[] localVerts )
    {
        int pos = 2;

        while ( pos < index.Length )
        {
            if ( value[index[pos]] != value[index[pos - 1]] )
                break;

            Vector2 vecPos1 = localVerts[index[pos - 1]];
            Vector2 vecPos2 = localVerts[index[pos]];

            if ( vecPos1.y > vecPos2.y || ( vecPos1.x > vecPos2.x && vecPos1.y == vecPos2.y ) )
            {
                Swap( index, pos, pos - 1 );

                if ( pos > 2 )
                    pos--;
                else
                    pos++;
            }
            else
                pos++;
        }
    }

    private static void SortForEnd( int[] index, int[] value, Vector2[] localVerts )
    {
        int count = index.Length;
        int pos = count - 2;

        while ( pos > 0 )
        {
            if ( value[index[pos]] != value[index[pos + 1]] )
                break;

            Vector2 vecPos1 = localVerts[index[pos]];
            Vector2 vecPos2 = localVerts[index[pos + 1]];

            if ( vecPos1.y < vecPos2.y )
            {
                Swap( index, pos, pos + 1 );

                if ( pos < count - 2 )
                    pos++;
                else
                    pos--;
            }
            else
                pos--;

        }
    }

    private static void Swap(int[] array, int a, int b)
    {
        int tmp = array[a];
        array[a] = array[b];
        array[b] = tmp;
    }
    
    private static bool SortLowY(Vector2 a, Vector2 b)
    {
        if (a.y > b.y)
            return false;
        else 
        if (a.y < b.y)
            return true;
        else 
        if (a.x < b.x)
            return true;
    
        return false;
    }
}