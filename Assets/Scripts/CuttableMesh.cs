﻿using UnityEngine;

public class CuttableMesh : MonoBehaviour
{
    public void Cut( Transform planeCutterTransform )
    {
        PlaneWrapper planeCutterMath = new PlaneWrapper( planeCutterTransform );

        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();

        for (int i = 0; i < meshFilters.Length; i++)
        {
            MeshWrapper meshWrapper = new MeshWrapper( meshFilters[i] );

            MeshCutter.Cut( meshWrapper, planeCutterMath );

            if ( meshWrapper.IsMeshSplit() )
            {
                SubMeshCreator.Create( meshWrapper, planeCutterMath, planeCutterTransform.rotation, MeshCutter.SubMeshIndecies );
                CreateNewObjects( meshWrapper );
            }
        }

        Destroy( planeCutterTransform.gameObject );
    }

    private void CreateNewObjects( MeshWrapper cutMeshWrapper )
    {
        for ( int i = 0; i < 2; i++ )
        {
            GameObject createdObject = new GameObject( string.Format( "{0}child{1}", cutMeshWrapper.transform.gameObject.name, i ) );

            createdObject.transform.parent = cutMeshWrapper.transform;

            createdObject.AddComponent<MeshFilter>().mesh = i == 0 ? cutMeshWrapper.CreateMeshRight() : cutMeshWrapper.CreateMeshLeft();

            MeshRenderer mr = createdObject.AddComponent<MeshRenderer>();
            mr.material = cutMeshWrapper.meshFilter.GetComponent<MeshRenderer>().material;

            createdObject.transform.Translate( ( i == 0 ? Vector3.right : Vector3.left ) * 2 );
        }

        Destroy( cutMeshWrapper.meshFilter );
        Destroy( cutMeshWrapper.meshFilter.GetComponent<MeshRenderer>() );
    }
}