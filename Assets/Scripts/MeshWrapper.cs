﻿using System.Collections.Generic;
using UnityEngine;

public class MeshWrapper
{
    public MeshFilter meshFilter;
    public Mesh mesh;
    public Transform transform;

    public int vertexCount;
    public Vector3[] wsVertices;
    public Vector3[] vertices;
    public Vector3[] normals;
    public Vector4[] tangents;
    public Vector2[] uv;
    public Vector2[] uv2;
    public Color[] colors;

    public int[] triangles;

    public List<Vector3> wsVerticesCut;
    public List<Vector3> verticesCut;
    public List<Vector3> normalsCut;
    public List<Vector4> tangentsCut;
    public List<Vector2> uvCut;
    public List<Vector2> uv2Cut;
    public List<Color> colorsCut;

    public List<int> trisRight = new List<int>();
    public List<int> trisLeft = new List<int>();

    public MeshWrapper( MeshFilter meshFilter )
    {
        this.meshFilter = meshFilter;
        mesh = meshFilter.mesh;
        transform = meshFilter.GetComponent<Transform>();

        vertexCount = mesh.vertexCount;
        vertices = mesh.vertices;
        wsVertices = worldSpaseVertices();
        triangles = mesh.triangles;
        normals = mesh.normals;
        tangents = mesh.tangents;
        uv = mesh.uv;
        uv2 = mesh.uv2;
        colors = mesh.colors;

        if ( wsVertices.Length != 0 )
            wsVerticesCut = new List<Vector3>();

        if ( vertices.Length != 0 )
            verticesCut = new List<Vector3>();

        if ( normals.Length != 0 )
            normalsCut = new List<Vector3>();

        if ( tangents.Length != 0 )
            tangentsCut = new List<Vector4>();

        if ( uv.Length != 0 )
            uvCut = new List<Vector2>();

        if ( uv2.Length != 0 )
            uv2Cut = new List<Vector2>();

        if ( colors.Length != 0 )
            colorsCut = new List<Color>();
    }
    
    public int AddLerpVertex( int from, int to, float t )
    {
        int index = vertexCount + verticesCut.Count;

        verticesCut.Add( Vector3.Lerp( vertices[from], vertices[to], t ) );
        wsVerticesCut.Add( Vector3.Lerp( wsVertices[from], wsVertices[to], t ) );

        if ( normalsCut != null )
            normalsCut.Add( Vector3.Lerp( normals[from], normals[to], t ) );

        if ( tangentsCut != null )
            tangentsCut.Add( Vector4.Lerp( tangents[from], tangents[to], t ) );

        if ( uvCut != null )
            uvCut.Add( Vector2.Lerp( uv[from], uv[to], t ) );

        if ( uv2Cut != null )
            uv2Cut.Add( Vector2.Lerp( uv2[from], uv2[to], t ) );

        if ( colorsCut != null )
            colorsCut.Add( Color.Lerp( colors[from], colors[to], t ) );
    
        return index;
    }
    
    public int AddSubMeshVertex( int refIndex, Vector3 normal )
    {
        if (uvCut != null)
        {
            if (refIndex >= vertexCount)
            {
                return AddSubMeshVertex( refIndex, normal, uvCut[refIndex - vertexCount] );
            }
            else
            {
                return AddSubMeshVertex( refIndex, normal, uv[refIndex] );
            }
        }
        else
        {
            return AddSubMeshVertex( refIndex, normal, Vector2.zero );
        }
    }
    
    public int AddSubMeshVertex( int refIndex, Vector3 normal, Vector2 capUV )
    {
        int index = vertexCount + verticesCut.Count;
        bool useArray = true;

        if ( refIndex >= vertexCount )
        {
            refIndex -= vertexCount;
            useArray = false;
        }
    
        if ( useArray )
        {
            verticesCut.Add( vertices[refIndex] );

            if ( uv2Cut != null )
                uv2Cut.Add( uv2[refIndex] );

            if ( colorsCut != null )
                colorsCut.Add( colors[refIndex] );
        }
        else
        {
            verticesCut.Add( verticesCut[refIndex] );

            if ( uv2Cut != null )
                uv2Cut.Add( uv2Cut[refIndex] );

            if ( colorsCut != null )
                colorsCut.Add( colorsCut[refIndex] );
        }

        if ( normalsCut != null )
            normalsCut.Add( normal );

        if ( uvCut != null )
            uvCut.Add( capUV );

        if ( tangentsCut != null )
        {
            Vector4 tangent = Vector4.zero;
            Vector3 c1 = Vector3.Cross( normal, Vector3.forward );
            Vector3 c2 = Vector3.Cross( normal, Vector3.up );

            if ( c1.sqrMagnitude > c2.sqrMagnitude )
            {
                tangent.x = c1.x;
                tangent.y = c1.y;
                tangent.z = c1.z;
            }
            else
            {
                tangent.x = c2.x;
                tangent.y = c2.y;
                tangent.z = c2.z;
            }

            tangentsCut.Add( tangent.normalized );
        }
    
        return index;
    }

    public bool IsMeshSplit()
    {
        return trisRight.Count > 0 && trisLeft.Count > 0;
    }

    public Mesh CreateMeshRight()
    {
        return CreateMesh( trisRight );
    }
    
    public Mesh CreateMeshLeft()
    {
        return CreateMesh( trisLeft );
    }
    
    private Mesh CreateMesh(List<int> tris)
    {
        int[] localTris = new int[tris.Count];
    
        int allVerticiesCount = vertexCount + verticesCut.Count;
        int[] translateIndex = new int[allVerticiesCount];

        for ( int i = 0; i < allVerticiesCount; i++ )
            translateIndex[i] = -1;
    
        int uniTriCount = 0;

        for ( int i = 0; i < tris.Count; i++ )
        {
            int triIndex = tris[i];
    
            if (translateIndex[triIndex] == -1)
            {
                translateIndex[triIndex] = uniTriCount++;
            }
    
            localTris[i] = translateIndex[triIndex];
        }
    
        Vector3[] localVerts = new Vector3[uniTriCount];
        Vector3[] localNormals = normals.Length != 0 ? new Vector3[uniTriCount] : normals;
        Vector4[] localTangents = tangents.Length != 0 ? new Vector4[uniTriCount] : tangents;
        Vector2[] localUv = uv.Length != 0 ? new Vector2[uniTriCount] : uv;
        Vector2[] localUv2 = uv2.Length != 0 ? new Vector2[uniTriCount] : uv2;
        Color[] localColors = colors.Length != 0 ? new Color[uniTriCount] : colors;
    
        uniTriCount = 0;

        for ( int i = 0; i < tris.Count; i++ )
        {
            int triIndex = tris[i];

            if ( translateIndex[triIndex] < uniTriCount )
                continue;

            if (triIndex < vertexCount)
            {
                localVerts[uniTriCount] = vertices[triIndex];

                if ( normalsCut != null )
                    localNormals[uniTriCount] = normals[triIndex];

                if ( tangentsCut != null )
                    localTangents[uniTriCount] = tangents[triIndex];

                if ( uvCut != null )
                    localUv[uniTriCount] = uv[triIndex];

                if ( uv2Cut != null )
                    localUv2[uniTriCount] = uv2[triIndex];

                if ( colorsCut != null )
                    localColors[uniTriCount] = colors[triIndex];
            }
            else
            {
                triIndex -= vertexCount;
                localVerts[uniTriCount] = verticesCut[triIndex];

                if ( normalsCut != null )
                    localNormals[uniTriCount] = normalsCut[triIndex];

                if ( tangentsCut != null )
                    localTangents[uniTriCount] = tangentsCut[triIndex];

                if ( uvCut != null )
                    localUv[uniTriCount] = uvCut[triIndex];

                if ( uv2Cut != null )
                    localUv2[uniTriCount] = uv2Cut[triIndex];

                if ( colorsCut != null )
                    localColors[uniTriCount] = colorsCut[triIndex];
            }
    
            uniTriCount++;
        }
    
        Mesh newMesh = new Mesh();
        newMesh.vertices = localVerts;
        newMesh.normals = localNormals;
        newMesh.tangents = localTangents;
        newMesh.uv = localUv;
        newMesh.uv2 = localUv2;
        newMesh.colors = localColors;
        newMesh.triangles = localTris;
    
        newMesh.RecalculateBounds();
    
        return newMesh;
    }

    private Vector3[] worldSpaseVertices()
    {
        Vector3[] wsVerts = new Vector3[vertices.Length];;
        Matrix4x4 localToWorld = transform.localToWorldMatrix;
        for ( int i = 0; i < vertices.Length; i++ )
        {
            wsVerts[i] = localToWorld.MultiplyPoint3x4( vertices[i] );
        }

        return wsVerts;
    }
}